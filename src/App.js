import React, { useState } from "react";
import Reorder, { reorder } from "react-reorder";

import "./App.css";

const App = () => {
  const pictures = [
    "https://cdn.iconscout.com/icon/free/png-256/react-1543566-1306069.png",
    "https://s3-ap-southeast-2.amazonaws.com/images.getjarvis.com.au/9068b2bd089c037a144fc847b9967ee83dce0fe5299261a884adc3241a33604b.jpeg",
    "https://s3-ap-southeast-2.amazonaws.com/images.getjarvis.com.au/9068b2bd089c037a144fc847b9967ee83dce0fe5299261a884adc3241a33604b.jpeg",
    "https://s3-ap-southeast-2.amazonaws.com/images.getjarvis.com.au/9068b2bd089c037a144fc847b9967ee83dce0fe5299261a884adc3241a33604b.jpeg",
    "https://s3-ap-southeast-2.amazonaws.com/images.getjarvis.com.au/9068b2bd089c037a144fc847b9967ee83dce0fe5299261a884adc3241a33604b.jpeg",
    "https://s3-ap-southeast-2.amazonaws.com/images.getjarvis.com.au/9068b2bd089c037a144fc847b9967ee83dce0fe5299261a884adc3241a33604b.jpeg"
  ];
  const [pictureList, setPictureList] = useState(pictures);

  const handleReorder = (e, prevIndex, nextIndex) => {
    if (window.confirm("Do you want to replace this image?")) {
      setPictureList(reorder(pictureList, prevIndex, nextIndex));
    }
  };

  return (
    <Reorder
      reorderId="my-list"
      component="ul"
      className="container"
      onReorder={handleReorder}
    >
      {pictureList.map((picture, index) => (
        <li key={index}>
          <img src={picture} alt="" />
        </li>
      ))}
    </Reorder>
  );
};

export default App;
